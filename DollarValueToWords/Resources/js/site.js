﻿//API Base URL
var baseUrl = "/api/";
var baseConverterUrl = baseUrl + "Convert/Get/";

//jQuery Validate
$.validator.addMethod("nameValidation", function (value, element, regexpr) {
    return regexpr.test(value);
}, "Please enter a valid name. '-_a-z and space");

$.validator.addMethod("amountValidation", function (value, element, regexpr) {
    return regexpr.test(value);
}, "Please enter the amount.");
$("#converterform").validate({
    rules: {
        name: {
            required: true,
            nameValidation: /^['-_ a-zA-Z]+$/
        },
        amount: {
            required: true,
            amountValidation: /^\d+(\.\d{0,2})?$/
        }
    },
    messages: {
        name: {
            required: "Please enter your name",
        },
        amount: {
            required: "Please enter the amount",
        }
    }
});

//On form Submit
$("#converterform").on("submit", function () {
    if ($(this).valid()) {
        url = baseConverterUrl + $("#name").val() + "/" + $("#amount").val() + "/";
        $.ajax({
            url: url, success: function (res) {
                var result = JSON.parse(res);
                $(".result").show();
                $(".result-name").html("Hi " + result.Name + "!");
                $(".result-words").html(result.AmountInWords);
            }
        });
    }
    return false;
});