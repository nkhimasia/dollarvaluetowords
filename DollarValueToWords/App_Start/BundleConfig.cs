﻿using System.Web;
using System.Web.Optimization;

namespace DollarValueToWords
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Resources/js/jquery-1.10.2.min.js",
                        "~/Resources/js/jquery.validate.min.js",
                        "~/Resources/js/site.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Resources/js/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Resources/css/bootstrap.min.css",
                      "~/Resources/css/site.css"));
        }
    }
}
