﻿namespace DollarValueToWords.Controllers
{
    using System.Web.Mvc;
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "AKQA .NET Tech Challenge";
            return View();
        }
    }
}
