﻿namespace DollarValueToWords.Controllers
{
    using Models;
    using System;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;

    public class ConvertController : Controller
    {
        public string Get(string name, string number)
        {
            Person p = new Person();
            p.Name = name;
            p.AmountInWords = DollarToWords(number);
            return new JavaScriptSerializer().Serialize(p);
        }

        private string DollarToWords(string value)
        {
            string _totalString = value;
            decimal _totalDecimal = Convert.ToDecimal(Convert.ToDecimal(_totalString).ToString("F"));
            int number = (int)_totalDecimal;
            int decimalValue = 0;

            string dollar = NumberToWords(number);
            string cents = string.Empty;

            if (_totalString.Contains("."))
            {
                decimalValue = int.Parse(_totalDecimal.ToString().Split('.')[1]);
                cents = NumberToWords(decimalValue);
            }
            string result = !string.IsNullOrEmpty(cents) ? string.Format("{0} DOLLAR AND {1} CENT", dollar, cents)+(decimalValue!=1?"S":"") : string.Format("{0} DOLLAR", dollar);
            return result;
        }

        private string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string _words = "";

            if ((number / 1000000000) > 0)
            {
                _words += NumberToWords(number / 1000000000) + " BILLION AND ";
                number %= 1000000000;
            }

            if ((number / 1000000) > 0)
            {
                _words += NumberToWords(number / 1000000) + " MILLION AND ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                _words += NumberToWords(number / 1000) + " THOUSAND AND ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                _words += NumberToWords(number / 100) + " HUNDRED AND ";
                number %= 100;
            }

            if (number > 0)
            {
                var _unitsMap = new[] { "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN" };
                var _tensMap = new[] { "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY" };

                if (number < 20)
                    _words += _unitsMap[number];
                else
                {
                    _words += _tensMap[(number) / 10];
                    if ((number % 10) > 0)
                        _words += "-" + _unitsMap[(number) % 10];
                }
            }
            return _words;
        }
    }
}