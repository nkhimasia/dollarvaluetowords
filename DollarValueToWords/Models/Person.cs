﻿namespace DollarValueToWords.Models
{
    // Person Model returns name and Dollars the person has
    public class Person
    {
        public string Name { get; set; }
        public string AmountInWords { get; set; }
    }
}