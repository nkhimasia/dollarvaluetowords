﻿namespace DollarValueToWords.Tests.Controllers
{
    using System.Web.Mvc;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using DollarValueToWords.Controllers;
    using Newtonsoft.Json;
    using Models;

    [TestClass]
    public class DollarValueToWordsTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("AKQA .NET Tech Challenge", result.ViewBag.Title);
        }

        [TestMethod]
        public void DollarToWordsAPI()
        {
            // Arrange
            ConvertController controller = new ConvertController();

            // Act
            Person result = JsonConvert.DeserializeObject<Person>(controller.Get("John Smith", "123.45"));

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("John Smith", result.Name);
            Assert.AreEqual("ONE HUNDRED AND TWENTY-THREE DOLLAR AND FORTY-FIVE CENTS", result.AmountInWords);
        }
    }
}
